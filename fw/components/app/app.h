/**
 * @file
 * @author Martin Stejskal
 * @brief Application layer for accelerometer analyzer
 */
#ifndef __APP_H__
#define __APP_H__
// ===============================| Includes |================================
#include <stdbool.h>
#include <stdint.h>

#include "sensors.h"
#include "ui_service_core.h"
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
typedef enum {
  APP_MODE_NONE = 0,              //!< Noting running
  APP_MODE_PRINT_RAW_DATA,        //!< Dummy print raw accelerometer data
  APP_MODE_TAP_DETECTION,         //!< Tap detection mode
  APP_MODE_ORIENTATION_TRACKING,  //!< Track roll and pitch
  APP_MODE_MAX,                   //!< Last item
} te_app_mode;
// ===========================| Global variables |============================

// ===============================| Functions |===============================
// =========================| High level functions |==========================
void app_task(void *pv_args);

/**
 * @brief Returns current mode
 *
 * @return APP_MODE_NONE if nothing is currently running. Otherwise follow
 *         description of @ref te_app_mode
 */
te_app_mode app_get_mode(void);

/**
 * @brief Reset current settings to predefined
 *
 * Fail safe function when something goes wrong
 */
void app_reset_settings(void);
// ========================| Middle level functions |=========================
/**
 * @brief Start sampling mode
 *
 * @return USC_OK if no error
 */
te_usc_error_code app_sampling_start(void);

/**
 * @brief Stop sampling mode
 *
 * @return USC_OK if no error
 */
te_usc_error_code app_sampling_stop(void);

/**
 * @brief Start tap detection mode
 *
 * @return USC_OK if no error
 */
te_usc_error_code app_tap_detection_start(void);

/**
 * @brief Stop tap detection mode
 *
 * @return USC_OK if no error
 */
te_usc_error_code app_tap_detection_stop(void);

te_usc_error_code app_ori_start(void);
te_usc_error_code app_ori_stop(void);

// ==========================| Low level functions |==========================
/**
 * @brief Returns current sample rate for all modes
 *
 * @return Sample rate in ms
 */
uint16_t app_sampling_rate_get(void);

/**
 * @brief Set new sample rate for all modes
 *
 * @param u16_sampling_rate_ms Sample rate in ms
 */
void app_sampling_rate_set(const uint16_t u16_sampling_rate_ms);

/**
 * @brief Tap detection - get minimum vector length
 *
 * For more information, see @ref ts_sensor_util_tap_det_params
 *
 * @return Minimum vector length
 */
uint16_t app_tap_det_get_min_vector_len(void);

/**
 * @brief Tap detection - set new minimum vector length
 *
 * For more information, see @ref ts_sensor_util_tap_det_params
 *
 * @param u16_min_vector_length Minimum vector length
 */
void app_tap_det_set_min_vector_len(uint16_t u16_min_vector_length);

/**
 * @brief Tap detection - get maximum vector length
 *
 * For more information, see @ref ts_sensor_util_tap_det_params
 *
 * @return Maximum vector length
 */
uint16_t app_tap_det_get_max_vector_len(void);

/**
 * @brief Tap detection - set maximum vector length
 *
 * For more information, see @ref ts_sensor_util_tap_det_params
 *
 * @param u16_max_vector_length Maximum vector length
 */
void app_tap_det_set_max_vector_len(uint16_t u16_max_vector_length);

/**
 * @brief Tap detection - get minimum duration in ms
 *
 * For more information, see @ref ts_sensor_util_tap_det_params
 *
 * @return Minimum tap duration in ms
 */
uint16_t app_tap_det_get_min_duration(void);

/**
 * @brief Tap detection - set minimum duration in ms
 *
 * For more information, see @ref ts_sensor_util_tap_det_params
 *
 * @param u16_min_duration_ms Minimum tap duration in ms
 */
void app_tap_det_set_min_duration(uint16_t u16_min_duration_ms);

/**
 * @brief Tap detection - get maximum duration in ms
 *
 * For more information, see @ref ts_sensor_util_tap_det_params
 *
 * @return Maximum tap duration in ms
 */
uint16_t app_tap_det_get_max_duration(void);

/**
 * @brief Tap duration - set maximum duration in ms
 *
 * For more information, see @ref ts_sensor_util_tap_det_params
 *
 * @param u16_max_duration_ms Maximum tap duration in ms
 */
void app_tap_det_set_max_duration(uint16_t u16_max_duration_ms);

/**
 * @brief Tap duration - get hold off duration in ms
 *
 * For more information, see @ref ts_sensor_util_tap_det_params
 *
 * @return Hold off duration in ms
 */
uint16_t app_tap_det_get_hold_off(void);

/**
 * @brief Tap duration - set hold off duration in ms
 *
 * For more information, see @ref ts_sensor_util_tap_det_params
 *
 * @param u16_hold_off_ms Hold off duration in ms
 */
void app_tap_det_set_hold_off(uint16_t u16_hold_off_ms);

/**
 * @brief Orientation tracking - get target Theta angle in degrees
 *
 * For more information, see @ref ts_sensor_util_ori_det_params
 *
 * @return Target Theta angle in degrees
 */
int16_t app_ori_get_target_theta(void);

/**
 * @brief Orientation tracking - set target Thea angle in degrees
 *
 * For more information, see @ref ts_sensor_util_ori_det_params
 *
 * @param i16_target_theta_deg New target Theta angle in degrees
 */
void app_ori_set_target_theta(int16_t i16_target_theta_deg);

/**
 * @brief Orientation tracking - get target Phi angle in degrees
 *
 * For more information, see @ref ts_sensor_util_ori_det_params
 *
 * @return Target Phi angle in degrees
 */
int16_t app_ori_get_target_phi(void);

/**
 * @brief Orientation tracking - set target Phi angle in degrees
 *
 * For more information, see @ref ts_sensor_util_ori_det_params
 *
 * @param i16_target_phi_deg New target Phi angle in degrees
 */
void app_ori_set_target_phi(int16_t i16_target_phi_deg);

/**
 * @brief Orientation tracking - get tolerance for Theta angle in degrees
 *
 * For more information, see @ref ts_sensor_util_ori_det_params
 *
 * @return Target tolerance for Theta angle in degrees
 */
int16_t app_ori_get_tolerance_theta(void);

/**
 * @brief Orientation tracking - set tolerance for Theta angle in degrees
 *
 * For more information, see @ref ts_sensor_util_ori_det_params
 *
 * @param i16_tolerance_theta_deg New tolerance for Theta angle in degrees
 */
void app_ori_set_tolerance_theta(int16_t i16_tolerance_theta_deg);

/**
 * @brief Orientation tracking - get tolerance for Phi angle in degrees
 *
 * For more information, see @ref ts_sensor_util_ori_det_params
 *
 * @return Target tolerance for Phi angle in degrees
 */
int16_t app_ori_get_tolerance_phi(void);

/**
 * @brief Orientation tracking - set tolerance for Phi angle in degrees
 *
 * For more information, see @ref ts_sensor_util_ori_det_params
 *
 * @param i16_tolerance_phi_deg New tolerance for Phi angle in degrees
 */
void app_ori_set_tolerance_phi(int16_t i16_tolerance_phi_deg);

/**
 * @brief Orientation tracking - get ignore tolerance for Phi in degrees
 *
 * For more information, see @ref ts_sensor_util_ori_det_params
 *
 * @return Ignore tolerance for Phi angle in degrees
 */
uint16_t app_ori_get_ignore_phi_tolerance(void);

/**
 * @brief Orientation tracking - set ignore tolerance for Phi in degrees
 *
 * For more information, see @ref ts_sensor_util_ori_det_params
 *
 * @param u16_ignore_phi_tolerance_deg New ignore tolerance for Phi in degrees
 */
void app_ori_set_ignore_phi_tolerance(uint16_t u16_ignore_phi_tolerance_deg);

/**
 * @brief Orientation tracking - get latest measured Theta and Phi in degrees
 *
 * @param[out] pi16_theta_deg Theta angle in degrees will be written here
 * @param[out] pi16_phi_deg Phi angle in degrees will be written here
 */
void app_ori_get_latest_thetha_phi(int16_t *pi16_theta_deg,
                                   int16_t *pi16_phi_deg);

// ========================| Print related functions |========================
/**
 * @brief Translate mode enumeration into text
 *
 * @param e_mode Selected mode
 * @return Pointer to text
 */
const char *app_get_mode_str(te_app_mode e_mode);

#endif  // __APP_H__
