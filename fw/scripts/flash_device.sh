#/bin/bash
# Define default port
port="/dev/ttyUSB4"

# Get project directory, so everything can be done through absolute paths ->
# -> can call this script from anywhere
this_dir=$( dirname $0 )

# Backup actual directory
user_dir=$( pwd )

cd "$this_dir"/../

# If port is defined, use it. Otherwise use default one
if [ -z $1 ] ; then
  echo "Using default port "$port""
else
  port=$1
fi

docker run --rm \
  -v $PWD:/project -w /project \
  --device=$port espressif/idf idf.py flash

# Go back
cd $user_dir
