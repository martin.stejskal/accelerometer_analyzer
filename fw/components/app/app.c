/**
 * @file
 * @author Martin Stejskal
 * @brief Application layer for accelerometer analyzer
 */
// ===============================| Includes |================================
#include "app.h"

#include <assert.h>
#include <driver/uart.h>
#include <esp_log.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <stdbool.h>
#include <string.h>

#include "cfg.h"
#include "sensors.h"
#include "sensors_utils.h"
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
/**
 * @brief Shared variables within this module
 */
typedef struct {
  bool b_uart_driver_installed;
  te_app_mode e_mode;

  // Sampling mode - setting structure
  ts_sensor_util_sampling_args s_sampling;

  // Tap detection mode - setting structure
  ts_sensor_util_tap_det_args s_tap_det;

  // Orientation tracking mode - settings structure
  ts_sensor_util_ori_det_args s_ori_tracking;

  // Sample rate - common for all modes
  uint32_t u32_sampling_period_ms;
} s_app_runtime;

/**
 * @brief Constants used in this module
 */
typedef struct {
  char *pac_start_sampling;
  char *pac_stop_sampling;
  char *pac_start_tap_detection;
  char *pac_stop_tap_detection;
  char *pac_start_ori_tracking;
  char *pac_stop_ori_tracking;

  char *pac_ori_in_position;
  char *pac_ori_out_of_position;
} s_app_const;
// ===========================| Global variables |============================
/**
 * @brief Tag for logging system
 */
static const char *mp_tag = "app";

/**
 * @brief Most of constants at one place
 */
static const s_app_const ms_app_const = {
    .pac_start_sampling = "==== Sampling ====\n",
    .pac_stop_sampling = "==================\n",

    .pac_start_tap_detection = "==== Tap detection ====\n",
    .pac_stop_tap_detection = "=======================\n",

    .pac_start_ori_tracking = "==== Orientation tracking ====\n",
    .pac_stop_ori_tracking = "==============================\n",
    .pac_ori_in_position = "In position\n",
    .pac_ori_out_of_position = "Out of position\n",
};

/**
 * @brief Runtime variables shared by functions within this module
 */
static s_app_runtime ms_app_runtime = {
    .b_uart_driver_installed = false,
    .e_mode = APP_MODE_NONE,

    .u32_sampling_period_ms = 0,
};

// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
/**
 * @brief Callback function used when in sampling mode
 *
 * @param ps_data Pointer to measured data
 */
static void _get_sampling_data_cb(ts_sensor_utils_data *ps_data);

/**
 * @brief Callback function used when in tap detection mode
 *
 * No parameter given. It simply tells that tap was detected
 */
static void _tap_detected_cb(void);

static void _ori_tracking_cb(bool b_in_position);
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================
/**
 * @brief Transform sensor error code and set current mode
 *
 * If error code is not "OK", the current mode will be set to "none" in order
 * to reflect failure. Otherwise given mode will be set
 *
 * @param e_sensor_err Error code given by sensor module
 * @param e_mode Required mode change
 * @return Error code for UI service module
 */
static te_usc_error_code _get_usc_err_code_set_mode(
    const e_sensor_error e_sensor_err, const te_app_mode e_mode);

/**
 * @brief Initialize UART dedicated for printing raw data or debug information
 *
 * @return ESP_OK if no error
 */
static esp_err_t _init_high_performance_output(void);

/**
 * @brief Print string directly to the high performance output
 *
 * @param p_string String to be printed. Must be terminated by NULL character
 */
static void _print_on_high_performance_uart(const char *p_string);
// =========================| High level functions |==========================
void app_task(void *pv_args) {
  const esp_err_t e_err_code = _init_high_performance_output();

  if (e_err_code) {
    ESP_LOGE(mp_tag, "Application task failed due to UART");
  }

  // Finalize runtime configuration
  ms_app_runtime.s_sampling.pf_get_data = _get_sampling_data_cb;
  ms_app_runtime.s_tap_det.pf_tap_detected = _tap_detected_cb;
  ms_app_runtime.s_ori_tracking.pf_ori_changed = _ori_tracking_cb;
  app_reset_settings();

  // Exit this task gracefully
  vTaskDelete(0);
}

te_app_mode app_get_mode(void) { return ms_app_runtime.e_mode; }

void app_reset_settings(void) {
  sensors_utils_get_default_sampling_args(&ms_app_runtime.s_sampling);
  sensors_utils_get_default_tap_detect(&ms_app_runtime.s_tap_det);
  sensors_utils_get_default_ori_tracking(&ms_app_runtime.s_ori_tracking);

  // Use lowest possible delay -> highest possible s_sampling rate
  app_sampling_rate_set(1);
}
// ========================| Middle level functions |=========================
te_usc_error_code app_sampling_start(void) {
  _print_on_high_performance_uart(ms_app_const.pac_start_sampling);

  return _get_usc_err_code_set_mode(
      sensors_utils_sampling_start(ms_app_runtime.s_sampling),
      APP_MODE_PRINT_RAW_DATA);
}

te_usc_error_code app_sampling_stop(void) {
  const te_usc_error_code e_err_code =
      _get_usc_err_code_set_mode(sensors_utils_sampling_stop(), APP_MODE_NONE);

  // Need to print this "stop message" after stop whole machinery
  _print_on_high_performance_uart(ms_app_const.pac_stop_sampling);

  return e_err_code;
}

te_usc_error_code app_tap_detection_start(void) {
  _print_on_high_performance_uart(ms_app_const.pac_start_tap_detection);

  return _get_usc_err_code_set_mode(
      sensors_utils_tap_detection_start(ms_app_runtime.s_tap_det),
      APP_MODE_TAP_DETECTION);
}

te_usc_error_code app_tap_detection_stop(void) {
  const te_usc_error_code e_err_code = _get_usc_err_code_set_mode(
      sensors_utils_tap_detection_stop(), APP_MODE_NONE);

  _print_on_high_performance_uart(ms_app_const.pac_stop_tap_detection);

  return e_err_code;
}

te_usc_error_code app_ori_start(void) {
  _print_on_high_performance_uart(ms_app_const.pac_start_ori_tracking);

  return _get_usc_err_code_set_mode(
      sensors_utils_ori_tracking_start(ms_app_runtime.s_ori_tracking),
      APP_MODE_ORIENTATION_TRACKING);
}

te_usc_error_code app_ori_stop(void) {
  const te_usc_error_code e_err_code = _get_usc_err_code_set_mode(
      sensors_utils_ori_tracking_stop(), APP_MODE_NONE);

  _print_on_high_performance_uart(ms_app_const.pac_stop_ori_tracking);

  return e_err_code;
};

// ==========================| Low level functions |==========================
uint16_t app_sampling_rate_get(void) {
  return ms_app_runtime.u32_sampling_period_ms;
}

void app_sampling_rate_set(const uint16_t u16_sampling_rate_ms) {
  ms_app_runtime.u32_sampling_period_ms = u16_sampling_rate_ms;

  ms_app_runtime.s_sampling.u32_sampling_period_ms =
      (uint32_t)u16_sampling_rate_ms;

  ms_app_runtime.s_tap_det.u32_sampling_period_ms =
      (uint32_t)u16_sampling_rate_ms;
}

uint16_t app_tap_det_get_min_vector_len(void) {
  return ms_app_runtime.s_tap_det.s_params.u16_min_vector_length;
}

void app_tap_det_set_min_vector_len(uint16_t u16_min_vector_length) {
  // Do not care about value. Tap detection core should handle it
  ms_app_runtime.s_tap_det.s_params.u16_min_vector_length =
      u16_min_vector_length;
}

uint16_t app_tap_det_get_max_vector_len(void) {
  return ms_app_runtime.s_tap_det.s_params.u16_max_vector_length;
}

void app_tap_det_set_max_vector_len(uint16_t u16_max_vector_length) {
  // Do not care about value. Tap detection core should handle it
  ms_app_runtime.s_tap_det.s_params.u16_max_vector_length =
      u16_max_vector_length;
}

uint16_t app_tap_det_get_min_duration(void) {
  return ms_app_runtime.s_tap_det.s_params.u16_min_duration_ms;
}

void app_tap_det_set_min_duration(uint16_t u16_min_duration_ms) {
  // Do not care about value. Tap detection core should handle it
  ms_app_runtime.s_tap_det.s_params.u16_min_duration_ms = u16_min_duration_ms;
}

uint16_t app_tap_det_get_max_duration(void) {
  return ms_app_runtime.s_tap_det.s_params.u16_max_duration_ms;
}

void app_tap_det_set_max_duration(uint16_t u16_max_duration_ms) {
  // Do not care about value. Tap detection core should handle it
  ms_app_runtime.s_tap_det.s_params.u16_max_duration_ms = u16_max_duration_ms;
}

uint16_t app_tap_det_get_hold_off(void) {
  return ms_app_runtime.s_tap_det.s_params.u16_hold_off_ms;
}

void app_tap_det_set_hold_off(uint16_t u16_hold_off_ms) {
  // Do not care about value. Tap detection core should handle it
  ms_app_runtime.s_tap_det.s_params.u16_hold_off_ms = u16_hold_off_ms;
}

int16_t app_ori_get_target_theta(void) {
  return ms_app_runtime.s_ori_tracking.s_params.i16_target_theta_deg;
}

void app_ori_set_target_theta(int16_t i16_target_theta_deg) {
  // Do not care about value. Orientation tracking core should handle it
  ms_app_runtime.s_ori_tracking.s_params.i16_target_theta_deg =
      i16_target_theta_deg;
}

int16_t app_ori_get_target_phi(void) {
  return ms_app_runtime.s_ori_tracking.s_params.i16_target_phi_deg;
}
void app_ori_set_target_phi(int16_t i16_target_phi_deg) {
  // Do not care about value. Orientation tracking core should handle it
  ms_app_runtime.s_ori_tracking.s_params.i16_target_phi_deg =
      i16_target_phi_deg;
}

int16_t app_ori_get_tolerance_theta(void) {
  return ms_app_runtime.s_ori_tracking.s_params.i16_tolerance_theta_deg;
}
void app_ori_set_tolerance_theta(int16_t i16_tolerance_theta_deg) {
  // Do not care about value. Orientation tracking core should handle it
  ms_app_runtime.s_ori_tracking.s_params.i16_tolerance_theta_deg =
      i16_tolerance_theta_deg;
}

int16_t app_ori_get_tolerance_phi(void) {
  return ms_app_runtime.s_ori_tracking.s_params.i16_tolerance_phi_deg;
}

void app_ori_set_tolerance_phi(int16_t i16_tolerance_phi_deg) {
  // Do not care about value. Orientation tracking core should handle it
  ms_app_runtime.s_ori_tracking.s_params.i16_tolerance_phi_deg =
      i16_tolerance_phi_deg;
}

uint16_t app_ori_get_ignore_phi_tolerance(void) {
  return ms_app_runtime.s_ori_tracking.s_params.u16_ignore_phi_tolerance_deg;
}

void app_ori_set_ignore_phi_tolerance(uint16_t u16_ignore_phi_tolerance_deg) {
  ms_app_runtime.s_ori_tracking.s_params.u16_ignore_phi_tolerance_deg =
      u16_ignore_phi_tolerance_deg;
}

void app_ori_get_latest_thetha_phi(int16_t *pi16_theta_deg,
                                   int16_t *pi16_phi_deg) {
  assert(pi16_theta_deg);
  assert(pi16_phi_deg);

  e_sensor_error e_err_code =
      sensors_utils_ori_last_angles(pi16_theta_deg, pi16_phi_deg);

  if (e_err_code) {
    // If reading fails (low probability - it simply should not), at least
    // report it
    ESP_LOGE(mp_tag, "Oh boy, reading angles failed. Reason: %s",
             sensor_error_code_to_str(e_err_code));
  }
}

// ========================| Print related functions |========================
const char *app_get_mode_str(te_app_mode e_mode) {
  switch (e_mode) {
    case APP_MODE_NONE:
      return "none";
    case APP_MODE_PRINT_RAW_DATA:
      return "raw data";
    case APP_MODE_TAP_DETECTION:
      return "tap detection";
    case APP_MODE_ORIENTATION_TRACKING:
      return "orientation tracking";

    case APP_MODE_MAX:
      return "LAST VALUE IN ENUM";
  }

  // Should not happen
  return usc_txt_fail();
}

// ====================| Internal functions: high level |=====================
static void _get_sampling_data_cb(ts_sensor_utils_data *ps_data) {
  // It might happen, that due to timing, whole operation was already stopped,
  // but due to delay of printing, it might be still something in buffer. So
  // simply check if still running in s_sampling mode or not
  if (APP_MODE_PRINT_RAW_DATA == ms_app_runtime.e_mode) {
    // Temporary storage for generated text. Needed around 21 characters, but
    // just in case that text would increase, keep it bit bigger
    char ac_buffer[32];

    sprintf(ac_buffer, "%d, %d, %d\n", ps_data->u_d.s_vect.i16_x,
            ps_data->u_d.s_vect.i16_y, ps_data->u_d.s_vect.i16_z);

    _print_on_high_performance_uart(ac_buffer);
  }
}

static void _tap_detected_cb(void) {
  _print_on_high_performance_uart("Tap detected\n");
}

static void _ori_tracking_cb(bool b_in_position) {
  if (b_in_position) {
    _print_on_high_performance_uart(ms_app_const.pac_ori_in_position);
  } else {
    _print_on_high_performance_uart(ms_app_const.pac_ori_out_of_position);
  }
}
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
static te_usc_error_code _get_usc_err_code_set_mode(
    const e_sensor_error e_sensor_err, const te_app_mode e_mode) {
  // If fail, set mode to "none"
  ms_app_runtime.e_mode = APP_MODE_NONE;

  switch (e_sensor_err) {
    case SENSOR_OK:
      // Only if success, apply given mode
      ms_app_runtime.e_mode = e_mode;
      return USC_OK;
    case SENSOR_FAIL:
      return USC_ERROR;
    case SENSOR_TIMEOUT:
      return USC_ERROR_TIMEOUT;
    case SENSOR_EMPTY_POINTER:
      return USC_ERROR_EMPTY_POINTER;
    default:
      // All other types of errors
      return USC_ERROR;
  }
}

static esp_err_t _init_high_performance_output(void) {
  esp_err_t e_err_code = ESP_FAIL;
  // =================================| UART |==============================
  const uart_config_t uart_config = {.baud_rate = UI_SENSORS_UART_BAUDRATE,
                                     .data_bits = UART_DATA_8_BITS,
                                     .parity = UART_PARITY_DISABLE,
                                     .stop_bits = UART_STOP_BITS_1,
                                     .flow_ctrl = UART_HW_FLOWCTRL_DISABLE};
  // Try to remove driver if already installed
  if (ms_app_runtime.b_uart_driver_installed) {
    uart_driver_delete(SENSORS_UART_INTERFACE);
  }

  e_err_code = uart_param_config(SENSORS_UART_INTERFACE, &uart_config);
  if (e_err_code) {
    ESP_LOGE(mp_tag, "UART configuration failed");
    return e_err_code;
  }

  e_err_code =
      uart_set_pin(SENSORS_UART_INTERFACE, IO_SENSORS_TXD_PIN,
                   UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
  if (e_err_code) {
    ESP_LOGE(mp_tag, "UART pin set fail");
    return e_err_code;
  }

  e_err_code =
      uart_driver_install(SENSORS_UART_INTERFACE, UI_SENSORS_RX_BUFFER_SIZE,
                          SENSORS_TX_BUFFER_SIZE, 0, NULL, 0);
  if (e_err_code) {
    ESP_LOGE(mp_tag, "UART driver install failed: %d", e_err_code);
    return e_err_code;
  } else {
    ms_app_runtime.b_uart_driver_installed = true;
  }
  return ESP_OK;
}

static void _print_on_high_performance_uart(const char *p_string) {
  uart_write_bytes(SENSORS_UART_INTERFACE, p_string, strlen(p_string));
}
