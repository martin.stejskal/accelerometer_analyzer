/**
 * @file
 * @author Martin Stejskal
 * @brief Simple button handler
 */
#ifndef __BUTTON_H__
#define __BUTTON_H__
// ===============================| Includes |================================
#include "cfg.h"
// ================================| Defines |================================

// ============================| Default values |=============================
#ifndef BUTTON_EVENT_QUEUE_SIZE_ITEMS
/**
 * @brief Define number of items in event queue
 */
#define BUTTON_EVENT_QUEUE_SIZE_ITEMS (10)
#endif  // BUTTON_EVENT_QUEUE_SIZE_ITEMS

#ifndef BUTTON_DEBOUNCE_TIME_MS
/**
 * @brief De-bounce time in ms
 *
 * When detected button press, it is necessary to ignore more request from
 * button for some time in order to avoid false positive detections.
 */
#define BUTTON_DEBOUNCE_TIME_MS (500)
#endif  // BUTTON_DEBOUNCE_TIME_MS
// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
/**
 * @brief Callback structure for pressed button button
 */
typedef void (*tf_button_pressed_cb)(void);
// ===========================| Global variables |============================

// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief High level function for RTOS
 *
 * @param pv_args Can be empty or pointer to callback
 *        @ref tfButtonPressedCb
 */
void button_task_rtos(void *pv_args);
// ========================| Middle level functions |=========================
/**
 * @brief Set callback for action when button pressed
 * @param[in] pf_button_pressed Pointer to function. If NULL, it means
 *       "no callback" so when button will be pressed, no action will be
 *       performed
 */
void button_set_cb(tf_button_pressed_cb pf_button_pressed);
// ==========================| Low level functions |==========================

#endif  // __BUTTON_H__
