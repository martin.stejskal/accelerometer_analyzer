/**
 * @file
 * @author Martin Stejskal
 * @brief User/project specific module for UI service code
 */
// ===============================| Includes |================================

#include "ui_service_user.h"

#include <driver/gpio.h>
#include <driver/uart.h>
#include <esp_log.h>
#include <esp_system.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <string.h>

#include "app.h"
#include "cfg.h"
#include "ui_service_core.h"
#include "ui_service_utils.h"
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char* mp_tag = "UI service (u)";

static const ts_usc_cmd_cfg mas_user_cmds[] = {
    {
        .pac_command = "start",
        .pac_help = "Start sampling",
        .b_require_argument = false,
        .pf_cmd_callback = usu_cmd_sampling_start,
    },
    {
        .pac_command = "stop",
        .pac_help = "Stop sampling",
        .b_require_argument = false,
        .pf_cmd_callback = usu_cmd_sampling_stop,
    },
    {
        .pac_command = "tap start",
        .pac_help = "Start tap detection",
        .b_require_argument = false,
        .pf_cmd_callback = usu_cmd_tap_detection_start,
    },
    {
        .pac_command = "tap stop",
        .pac_help = "Stop tap detection",
        .b_require_argument = false,
        .pf_cmd_callback = usu_cmd_tap_detection_stop,
    },
    {
        .pac_command = "tap get",
        .pac_help = "Get all tap detection parameters",
        .b_require_argument = false,
        .pf_cmd_callback = usu_cmd_tap_detection_get,
    },
    {
        .pac_command = "tap set min thr",
        .pac_help = "Set minimum tap threshold as raw vector size",
        .b_require_argument = true,
        .pf_cmd_callback = usu_cmd_set_min_tap_thrshld,
        .s_arg_cfg = {.e_arg_type = UI_SRVCE_CMD_ARG_TYPE_INT,
                      .i32_min = 0,
                      .i32_max = 0xFFFF,
                      .pac_argument_example = "500"},
    },
    {
        .pac_command = "tap set max thr",
        .pac_help = "Set maximum tap threshold as raw vector size",
        .b_require_argument = true,
        .pf_cmd_callback = usu_cmd_set_max_tap_thrshld,
        .s_arg_cfg = {.e_arg_type = UI_SRVCE_CMD_ARG_TYPE_INT,
                      .i32_min = 0,
                      .i32_max = 0xFFFF,
                      .pac_argument_example = "1700"},
    },
    {
        .pac_command = "tap set min dur",
        .pac_help = "Set minimum tap duration in ms",
        .b_require_argument = true,
        .pf_cmd_callback = usu_cmd_set_min_tap_duration,
        .s_arg_cfg = {.e_arg_type = UI_SRVCE_CMD_ARG_TYPE_INT,
                      .i32_min = 0,
                      .i32_max = 0xFFFF,
                      .pac_argument_example = "10"},
    },
    {
        .pac_command = "tap set max dur",
        .pac_help = "Set maximum tap duration in ms",
        .b_require_argument = true,
        .pf_cmd_callback = usu_cmd_set_max_tap_duration,
        .s_arg_cfg = {.e_arg_type = UI_SRVCE_CMD_ARG_TYPE_INT,
                      .i32_min = 0,
                      .i32_max = 0xFFFF,
                      .pac_argument_example = "40"},
    },
    {
        .pac_command = "tap set hold off",
        .pac_help =
            "Set hold off time in ms. Used when shock above threshold is "
            "detected, so algorithm will ignore new data for some time",
        .b_require_argument = true,
        .pf_cmd_callback = usu_cmd_set_tap_hold_off,
        .s_arg_cfg = {.e_arg_type = UI_SRVCE_CMD_ARG_TYPE_INT,
                      .i32_min = 0,
                      .i32_max = 0xFFFF,
                      .pac_argument_example = "800"},
    },
    {
        .pac_command = "ori start",
        .pac_help = "Start orientation tracking mode",
        .b_require_argument = false,
        .pf_cmd_callback = usu_cmd_start_ori_tracking,
    },
    {
        .pac_command = "ori stop",
        .pac_help = "Stop orientation tracking mode",
        .b_require_argument = false,
        .pf_cmd_callback = usu_cmd_stop_ori_tracking,
    },
    {
        .pac_command = "ori get",
        .pac_help = "Get all orientation tracking settings at once",
        .b_require_argument = false,
        .pf_cmd_callback = usu_cmd_ori_get,
    },
    {
        .pac_command = "ori latest",
        .pac_help = "Get latest measurement of Theta and Phi angle",
        .b_require_argument = false,
        .pf_cmd_callback = usu_cmd_ori_latest,
    },
    {
        .pac_command = "ori set th",
        .pac_help = "Set target Theta angle for orientation tracking mode",
        .b_require_argument = true,
        .pf_cmd_callback = usu_cmd_set_target_theta,
        .s_arg_cfg =
            {
                .e_arg_type = UI_SRVCE_CMD_ARG_TYPE_INT,
                .i32_min = 0,
                .i32_max = 180,
                .pac_argument_example = "45",
            },
    },
    {
        .pac_command = "ori set phi",
        .pac_help = "Set target Phi angle for orientation tracking mode",
        .b_require_argument = true,
        .pf_cmd_callback = usu_cmd_set_target_phi,
        .s_arg_cfg =
            {
                .e_arg_type = UI_SRVCE_CMD_ARG_TYPE_INT,
                .i32_min = -360,
                .i32_max = 360,
                .pac_argument_example = "-15",
            },
    },
    {
        .pac_command = "ori set tol th",
        .pac_help = "Set tolerance for Theta in target position",
        .b_require_argument = true,
        .pf_cmd_callback = usu_cmd_set_tolerance_theta,
        .s_arg_cfg =
            {
                .e_arg_type = UI_SRVCE_CMD_ARG_TYPE_INT,
                .i32_min = 0,
                .i32_max = 180,
                .pac_argument_example = "35",
            },
    },
    {
        .pac_command = "ori set tol phi",
        .pac_help = "Set tolerance for Phi in target position",
        .b_require_argument = true,
        .pf_cmd_callback = usu_cmd_set_tolerance_phi,
        .s_arg_cfg =
            {
                .e_arg_type = UI_SRVCE_CMD_ARG_TYPE_INT,
                .i32_min = 0,
                .i32_max = 180,
                .pac_argument_example = "10",
            },
    },
    {
        .pac_command = "ori set ign phi",
        .pac_help = "Set ignore tolerance for phi (useful when phi can not be "
                    "calculated accurately - this is mathematical issue)",
        .b_require_argument = true,
        .pf_cmd_callback = usu_cmd_set_ignore_phi_tolerance,
        .s_arg_cfg = {.e_arg_type = UI_SRVCE_CMD_ARG_TYPE_INT,
                      .i32_min = 0,
                      .i32_max = 89,
                      .pac_argument_example = "5,"},
    },
    {
        .pac_command = "status",
        .pac_help = "Tells if application is running or not",
        .b_require_argument = false,
        .pf_cmd_callback = usu_cmd_get_status,
    },
    {
        .pac_command = "get sample rate",
        .pac_help = "Get actual sample rate in ms",
        .b_require_argument = false,
        .pf_cmd_callback = usu_cmd_get_sampling_rate,
    },
    {
        .pac_command = "set sample rate",
        .pac_help = "Set new sample rate in ms. Apply for all modes",
        .b_require_argument = true,
        .pf_cmd_callback = usu_cmd_set_sampling_rate,

        .s_arg_cfg = {.e_arg_type = UI_SRVCE_CMD_ARG_TYPE_INT,
                      .i32_min = 1,
                      .i32_max = 0xFFFF,
                      .pac_argument_example = "50"},
    },
    {
        .pac_command = "get hw",
        .pac_help = "Returns current HW setup: GPIO setup, bit rates and so on",
        .b_require_argument = false,
        .pf_cmd_callback = usu_cmd_get_hw_params,
    },
    {
        .pac_command = "reset",
        .pac_help = "Reset all settings to default state",
        .b_require_argument = false,
        .pf_cmd_callback = usu_cmd_reset_settings,
    },
    {
        .pac_command = "reboot",
        .pac_help = "Reboot system via software reset",
        .b_require_argument = false,
        .pf_cmd_callback = usu_cmd_reboot,
    },
};

static const char* pac_fail_already_runnig = "[FAIL] Already running";
static const char* pac_fail_already_stopped = "[FAIL] Already stopped";
static const char* pac_fail_run_in_different_mode =
    "[FAIL] Running in different mode";

/**
 * @brief Keep track about UART driver state
 */
static bool mb_uart_driver_installed = false;
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
const ts_usc_cmd_cfg* usu_get_list_of_cmds(void) { return mas_user_cmds; }

uint16_t usu_get_num_of_cmds(void) {
  return (sizeof(mas_user_cmds) / sizeof(mas_user_cmds[0]));
}

void usu_delay_ms(const uint16_t u16_delay_ms) {
  const TickType_t x_tick_delay = u16_delay_ms / portTICK_PERIOD_MS;
  if (x_tick_delay) {
    vTaskDelay(x_tick_delay);
  } else {
    // If delay value is zero, wait at least 1 tick
    vTaskDelay(1);
  }
}

// ===============================| Commands |================================
void usu_cmd_get_status(ts_usc_cb_args* pc_cb_args) {
  const te_app_mode e_mode = app_get_mode();
  assert(e_mode < APP_MODE_MAX);

  if (e_mode > APP_MODE_NONE) {
    // Something is running
    usc_printf("Running mode \"%s\"", app_get_mode_str(e_mode));
  } else {
    usc_print_str("Stopped");
  }
}

void usu_cmd_sampling_start(ts_usc_cb_args* ps_cb_args) {
  const te_app_mode e_mode = app_get_mode();
  assert(e_mode < APP_MODE_MAX);

  if (APP_MODE_NONE == e_mode) {
    // Ready to start
    usc_print_ok_fail(app_sampling_start());

  } else if (APP_MODE_PRINT_RAW_DATA == e_mode) {
    // Already running -> inform user
    usc_print_str(pac_fail_already_runnig);
  } else {
    usc_print_str(pac_fail_run_in_different_mode);
  }
}

void usu_cmd_sampling_stop(ts_usc_cb_args* pc_cb_args) {
  const te_app_mode e_mode = app_get_mode();
  assert(e_mode < APP_MODE_MAX);

  if (APP_MODE_PRINT_RAW_DATA == e_mode) {
    // Still running & correct mode
    usc_print_ok_fail(app_sampling_stop());

  } else if (e_mode != APP_MODE_NONE) {
    // Trying to stop different mode
    usc_print_str(pac_fail_run_in_different_mode);

  } else {
    // Already stopped
    usc_print_str(pac_fail_already_stopped);
  }
}

void usu_cmd_tap_detection_start(ts_usc_cb_args* pc_cb_args) {
  const te_app_mode e_mode = app_get_mode();
  assert(e_mode < APP_MODE_MAX);

  if (APP_MODE_NONE == e_mode) {
    // Ready to start
    usc_print_ok_fail(app_tap_detection_start());
  } else if (APP_MODE_TAP_DETECTION == e_mode) {
    // Already running -> inform user
    usc_print_str(pac_fail_already_runnig);
  } else {
    usc_print_str(pac_fail_run_in_different_mode);
  }
}

void usu_cmd_tap_detection_stop(ts_usc_cb_args* pc_cb_args) {
  const te_app_mode e_mode = app_get_mode();
  assert(e_mode < APP_MODE_MAX);

  if (APP_MODE_TAP_DETECTION == e_mode) {
    // Still running & correct mode
    usc_print_ok_fail(app_tap_detection_stop());

  } else if (e_mode != APP_MODE_NONE) {
    // Trying to stop different mode
    usc_print_str(pac_fail_run_in_different_mode);

  } else {
    // Already stopped
    usc_print_str(pac_fail_already_stopped);
  }
}

void usu_cmd_tap_detection_get(ts_usc_cb_args* psCbArgs) {
  usc_printf(
      "Minimum threshold: %u\n"
      "Maximum threshold: %u\n"
      "Minimum duration:  %u ms\n"
      "Maximum duration:  %u ms\n"
      "Hold off:          %u ms\n",
      app_tap_det_get_min_vector_len(), app_tap_det_get_max_vector_len(),
      app_tap_det_get_min_duration(), app_tap_det_get_max_duration(),
      app_tap_det_get_hold_off());
}

void usu_cmd_set_min_tap_thrshld(ts_usc_cb_args* pc_cb_args) {
  // No need to check for mode or if running or not. Can be set at any time
  app_tap_det_set_min_vector_len(pc_cb_args->u_value.u32_value);

  // Always successful
  usc_print_ok_fail(USC_OK);
}

void usu_cmd_set_max_tap_thrshld(ts_usc_cb_args* pc_cb_args) {
  // No need to check for mode or if running or not. Can be set at any time
  app_tap_det_set_max_vector_len(pc_cb_args->u_value.u32_value);

  // Always successful
  usc_print_ok_fail(USC_OK);
}

void usu_cmd_set_min_tap_duration(ts_usc_cb_args* pc_cb_args) {
  // No need to check for mode or if running or not. Can be set at any time
  app_tap_det_set_min_duration(pc_cb_args->u_value.u32_value);

  // Always successful
  usc_print_ok_fail(USC_OK);
}

void usu_cmd_set_max_tap_duration(ts_usc_cb_args* pc_cb_args) {
  // No need to check for mode or if running or not. Can be set at any time
  app_tap_det_set_max_duration(pc_cb_args->u_value.u32_value);

  // Always successful
  usc_print_ok_fail(USC_OK);
}

void usu_cmd_set_tap_hold_off(ts_usc_cb_args* pc_cb_args) {
  // No need to check for mode or if running or not. Can be set at any time
  app_tap_det_set_hold_off(pc_cb_args->u_value.u32_value);

  // Always successful
  usc_print_ok_fail(USC_OK);
}

void usu_cmd_start_ori_tracking(ts_usc_cb_args* pc_cb_args) {
  const te_app_mode e_mode = app_get_mode();
  assert(e_mode < APP_MODE_MAX);

  if (APP_MODE_NONE == e_mode) {
    // Ready to start
    usc_print_ok_fail(app_ori_start());
  } else if (APP_MODE_ORIENTATION_TRACKING) {
    // Already running -> inform user
    usc_print_str(pac_fail_already_runnig);
  } else {
    usc_print_str(pac_fail_run_in_different_mode);
  }
}

void usu_cmd_stop_ori_tracking(ts_usc_cb_args* pc_cb_args) {
  const te_app_mode e_mode = app_get_mode();
  assert(e_mode < APP_MODE_MAX);

  if (APP_MODE_ORIENTATION_TRACKING == e_mode) {
    // Still running & correct mode
    usc_print_ok_fail(app_ori_stop());

  } else if (e_mode != APP_MODE_NONE) {
    // Trying to stop different mode
    usc_print_str(pac_fail_run_in_different_mode);

  } else {
    // Already stopped
    usc_print_str(pac_fail_already_stopped);
  }
}

void usu_cmd_ori_get(ts_usc_cb_args* psCbArgs) {
  usc_printf(
      "Target theta:         %d deg\n"
      "Target phi:           %d deg\n"
      "Tolerance theta:      %d deg\n"
      "Tolerance phi:        %d deg\n"
      "Ignore phi tolerance: %d deg\n",
      app_ori_get_target_theta(), app_ori_get_target_phi(),
      app_ori_get_tolerance_theta(), app_ori_get_tolerance_phi(),
      app_ori_get_ignore_phi_tolerance());
}

void usu_cmd_ori_latest(ts_usc_cb_args* psCbArgs) {
  int16_t i16_theta, i16_phi;

  app_ori_get_latest_thetha_phi(&i16_theta, &i16_phi);

  usc_printf(
      "Thetha: %d deg\n"
      "Phi:    %d deg\n",
      i16_theta, i16_phi);
}

void usu_cmd_set_target_theta(ts_usc_cb_args* pc_cb_args) {
  // No need to check for mode or if running or not. Can be set at any time
  app_ori_set_target_theta(pc_cb_args->u_value.i32_value);

  // Always successful
  usc_print_ok_fail(USC_OK);
}

void usu_cmd_set_target_phi(ts_usc_cb_args* pc_cb_args) {
  // No need to check for mode or if running or not. Can be set at any time
  app_ori_set_target_phi(pc_cb_args->u_value.i32_value);

  // Always successful
  usc_print_ok_fail(USC_OK);
}

void usu_cmd_set_tolerance_theta(ts_usc_cb_args* pc_cb_args) {
  // No need to check for mode or if running or not. Can be set at any time
  app_ori_set_tolerance_theta(pc_cb_args->u_value.i32_value);

  // Always successful
  usc_print_ok_fail(USC_OK);
}

void usu_cmd_set_tolerance_phi(ts_usc_cb_args* pc_cb_args) {
  // No need to check for mode or if running or not. Can be set at any time
  app_ori_set_tolerance_phi(pc_cb_args->u_value.i32_value);

  // Always successful
  usc_print_ok_fail(USC_OK);
}

void usu_cmd_set_ignore_phi_tolerance(ts_usc_cb_args* pc_cb_args) {
  // No need to check for mode or if running or not. Can be set at any time
  app_ori_set_ignore_phi_tolerance(pc_cb_args->u_value.i32_value);

  // Always successful
  usc_print_ok_fail(USC_OK);
}

void usu_cmd_get_sampling_rate(ts_usc_cb_args* pc_cb_args) {
  usc_printf("%d ms", app_sampling_rate_get());
}

void usu_cmd_set_sampling_rate(ts_usc_cb_args* pc_cb_args) {
  // Should fit into 16 bit variable
  assert(pc_cb_args->u_value.u32_value <= UINT16_MAX);

  app_sampling_rate_set(pc_cb_args->u_value.u32_value);

  // Always successful
  usc_print_ok_fail(USC_OK);
}

void usu_cmd_get_hw_params(ts_usc_cb_args* pc_cb_args) {
  usc_printf("== I2C ==\n SDA: GPIO%d\n SCL: GPIO%d\n Bit rate: %d bps\n\n",
             IO_I2C_SDA, IO_I2C_SCL, I2C_SCL_SPEED);

  usc_printf("== Sensors output ==\n UART: TX GPIO%d\n Bit rate: %d bps\n\n",
             IO_SENSORS_TXD_PIN, UI_SENSORS_UART_BAUDRATE);

  usc_printf(
      "== ESP log output ==\n UART: TX GPIO1\n Bit rate: 115200 bps\n\n");

  usc_printf("== Button ==\n GPIO%d\n\n", IO_START_STOP_BTN);

  usc_printf("== LED ==\n GPIO%d\n\n", IO_LED);

  // Printing setup for UI service does not make sense, since already user
  // can read it out or it will be turned into garbage anyway
}

void usu_cmd_reset_settings(ts_usc_cb_args* psCbArgs) { app_reset_settings(); }

void usu_cmd_reboot(ts_usc_cb_args* psCbArgs) { esp_restart(); }

// ===========================| Interface related |===========================
te_usc_error_code usu_uart_init(void) {
  esp_err_t e_err_code;
  // =================================| UART |==============================
  const uart_config_t s_uart_config = {.baud_rate = UI_SERVICE_UART_BAUDRATE,
                                       .data_bits = UART_DATA_8_BITS,
                                       .parity = UART_PARITY_DISABLE,
                                       .stop_bits = UART_STOP_BITS_1,
                                       .flow_ctrl = UART_HW_FLOWCTRL_DISABLE};
  // Try to remove driver if already installed
  if (mb_uart_driver_installed) {
    uart_driver_delete(UI_SERVICE_UART_INTERFACE);
  }

  e_err_code = uart_param_config(UI_SERVICE_UART_INTERFACE, &s_uart_config);
  if (e_err_code) {
    ESP_LOGE(mp_tag, "UART configuration failed");
    return ESP_FAIL;
  }

  e_err_code =
      uart_set_pin(UI_SERVICE_UART_INTERFACE, UI_SERVICE_TXD_PIN,
                   UI_SERVICE_RXD_PIN, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
  if (e_err_code) {
    ESP_LOGE(mp_tag, "UART pin set fail");
    return ESP_FAIL;
  }

  e_err_code = uart_driver_install(UI_SERVICE_UART_INTERFACE,
                                   UI_SERVICE_ITF_BUFFER_SIZE, 0, 0, NULL, 0);
  if (e_err_code) {
    ESP_LOGE(mp_tag, "UART driver install failed: %d", e_err_code);
    return ESP_FAIL;
  } else {
    mb_uart_driver_installed = true;
  }
  return ESP_OK;
}

te_usc_error_code usu_uart_read(uint8_t* pu8_data,
                                const uint16_t u16_buffer_length,
                                const uint16_t u16_timeout_ms,
                                uint16_t* pu_num_of_read_bytes) {
  int i_rx_bytes_cnd =
      uart_read_bytes(UI_SERVICE_UART_INTERFACE, pu8_data, u16_buffer_length,
                      u16_timeout_ms / portTICK_PERIOD_MS);

  // Really does not expect to receive more than 65k of data in 1 batch
  assert(i_rx_bytes_cnd <= 0xFFFF);

  *pu_num_of_read_bytes = i_rx_bytes_cnd;

  return USC_OK;
}

te_usc_error_code usu_uart_write(const uint8_t* pu8_data,
                                 const uint16_t u16_data_length,
                                 const uint16_t u16_timeout_ms) {
  int i_tx_bytes =
      uart_write_bytes(UI_SERVICE_UART_INTERFACE, pu8_data, u16_data_length);

  if (i_tx_bytes != u16_data_length) {
    ESP_LOGE(mp_tag, "Transmitted %d but expected to transmit %d", i_tx_bytes,
             u16_data_length);
    return USC_ERROR;
  }

  TickType_t ticks_to_wait = u16_timeout_ms / portTICK_PERIOD_MS;
  // Should not be zero. At least 1 tick should be given
  if (ticks_to_wait == 0) {
    ticks_to_wait = 1;
  }

  esp_err_t e_err_uart_hal =
      uart_wait_tx_done(UI_SERVICE_UART_INTERFACE, ticks_to_wait);

  if (e_err_uart_hal == ESP_ERR_TIMEOUT) {
    return USC_ERROR_TIMEOUT;

  } else if (e_err_uart_hal) {
    return USC_ERROR;

  } else {
    return USC_OK;
  }
}

te_usc_error_code usu_uart_deinit(void) {
  // Try to remove driver no matter what
  uart_driver_delete(UI_SERVICE_UART_INTERFACE);

  // Disable also GPIO
  gpio_pad_select_gpio(UI_SERVICE_TXD_PIN);
  gpio_pad_select_gpio(UI_SERVICE_RXD_PIN);

  gpio_config_t sCfg = {
      .pin_bit_mask = BIT64(UI_SERVICE_TXD_PIN) | BIT64(UI_SERVICE_RXD_PIN),
      .mode = GPIO_MODE_DISABLE,
      .pull_up_en = false,
      .pull_down_en = false,
      .intr_type = GPIO_INTR_DISABLE,
  };
  gpio_config(&sCfg);

  return USC_OK;
}

// ==========================| Internal functions |===========================
