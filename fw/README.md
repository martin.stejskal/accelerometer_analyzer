# About

* Device use all 3 UART interfaces on ESP32.
* One is used for *service UI*, another is there for printing *raw data* and
  last, but not least, is there for *programming*.
* You probably figured out, how to program ESP32, so description of programming
  UART is skipped. If not, please see official documentation from
  [Espressif](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/index.html)

# Service UI

* Report HW configuration and allow to configure device. You can start here
* UART
  * `115200-8-N-1`
  * TX (MCU side): `GPIO17`
  * RX (MCU side): `GPIO16`

* Just press *enter* and help will be displayed. Command `get hw` will print
  current HW settings (pin mapping, bit rates). This basically dynamically
  tells you HW configuration even if you change it ;)

# Troubleshooting

## Device is resetting

* Sensor is not detected on I2C bus. Double check wiring. Also bitrate on I2C
  is quite high, so make sure that wires are not too long (15 cm should be OK).
