/**
 * @file
 * @author Martin Stejskal
 * @brief User/project specific module for UI service code
 */
#ifndef __UI_SERVICE_USER_H__
#define __UI_SERVICE_USER_H__
// ===============================| Includes |================================
#include <esp_err.h>

#include "ui_service_core.h"
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
/**
 * @brief Returns list of available commands
 * @return List of commands as pointer to static list
 */
const ts_usc_cmd_cfg *usu_get_list_of_cmds(void);

/**
 * @brief Returns number of available commands
 * @return Number of available commands
 */
uint16_t usu_get_num_of_cmds(void);

/**
 * @brief Delay function for UI service core
 * @param u16_delay_ms Input delay in milliseconds
 */
void usu_delay_ms(const uint16_t u16_delay_ms);

// ===============================| Commands |================================
/**
 * @brief For command description, see @ref mas_user_cmds
 *
 * @{
 */
void usu_cmd_get_status(ts_usc_cb_args *psCbArgs);

void usu_cmd_sampling_start(ts_usc_cb_args *psCbArgs);
void usu_cmd_sampling_stop(ts_usc_cb_args *psCbArgs);

void usu_cmd_tap_detection_start(ts_usc_cb_args *psCbArgs);
void usu_cmd_tap_detection_stop(ts_usc_cb_args *psCbArgs);

void usu_cmd_tap_detection_get(ts_usc_cb_args *psCbArgs);

void usu_cmd_set_min_tap_thrshld(ts_usc_cb_args *psCbArgs);
void usu_cmd_set_max_tap_thrshld(ts_usc_cb_args *psCbArgs);
void usu_cmd_set_min_tap_duration(ts_usc_cb_args *psCbArgs);
void usu_cmd_set_max_tap_duration(ts_usc_cb_args *psCbArgs);
void usu_cmd_set_tap_hold_off(ts_usc_cb_args *psCbArgs);

void usu_cmd_start_ori_tracking(ts_usc_cb_args *psCbArgs);
void usu_cmd_stop_ori_tracking(ts_usc_cb_args *psCbArgs);

void usu_cmd_ori_get(ts_usc_cb_args *psCbArgs);

void usu_cmd_ori_latest(ts_usc_cb_args *psCbArgs);

void usu_cmd_set_target_theta(ts_usc_cb_args *psCbArgs);
void usu_cmd_set_target_phi(ts_usc_cb_args *psCbArgs);
void usu_cmd_set_tolerance_theta(ts_usc_cb_args *psCbArgs);
void usu_cmd_set_tolerance_phi(ts_usc_cb_args *psCbArgs);
void usu_cmd_set_ignore_phi_tolerance(ts_usc_cb_args *pc_cb_args);

void usu_cmd_get_sampling_rate(ts_usc_cb_args *psCbArgs);
void usu_cmd_set_sampling_rate(ts_usc_cb_args *pc_cb_args);

void usu_cmd_get_hw_params(ts_usc_cb_args *psCbArgs);

void usu_cmd_reset_settings(ts_usc_cb_args *psCbArgs);
void usu_cmd_reboot(ts_usc_cb_args *psCbArgs);
/**
 * @}
 */
// ===========================| Interface related |===========================
/**
 * @brief Initialize service user interface
 *
 * @return USC_OK if no error
 */
te_usc_error_code usu_uart_init(void);

/**
 * @brief API for reading from service UI interface
 *
 * @param[out] pu8_data Read data will be written here
 * @param u16_buffer_length Define data buffer length in Bytes
 * @param u16_timeout_ms Maximum acceptable timeout when reading from interface
 * @param[out] pu_num_of_read_bytes Returns number of read Bytes
 * @return USC_OK if no error
 */
te_usc_error_code usu_uart_read(uint8_t *pu8_data,
                                const uint16_t u16_buffer_length,
                                const uint16_t u16_timeout_ms,
                                uint16_t *pu_num_of_read_bytes);

/**
 * @brief API for writing to service UI interface
 *
 * @param[in] pu8_data Data will be read from there
 * @param u16_data_length Define write data length
 * @param u16_timeout_ms Maximum acceptable timeout when writing to interface
 * @return USC_OK if no error
 */
te_usc_error_code usu_uart_write(const uint8_t *pu8_data,
                                 const uint16_t u16_data_length,
                                 const uint16_t u16_timeout_ms);

/**
 * @brief Deinitialize service user interface
 *
 * This allow to reuse interface for other functionality if needed
 *
 * @return USC_OK if no error
 */
te_usc_error_code usu_uart_deinit(void);

#endif  // __UI_SERVICE_USER_H__
