/**
 * @file
 * @author Martin Stejskal
 * @brief Configuration file for Anomaly meter
 *
 * Mainly here is HW configuration
 */
#ifndef __CFG_H__
#define __CFG_H__
// ===============================| Includes |================================
#include <driver/adc.h>
#include <esp_log.h>
// ================================| Defines |================================
/**
 * @brief Protocol CPU
 */
#define PRO_CPU (0)

/**
 * @brief Application CPU
 */
#define APP_CPU (1)
// ==================================| I/O |==================================
/**
 * @brief I2C GPIO mapping
 *
 * @{
 */
///@brief I2C data
#define IO_I2C_SDA (21)

///@brief I2C clock
#define IO_I2C_SCL (22)

/**
 * @}
 */

/**
 * @brief UART for measured data
 *
 * @{
 */
#define IO_SENSORS_TXD_PIN (23)

/**
 * @}
 */

/**
 * @brief Start/stop button IO
 */
#define IO_START_STOP_BTN (27)

/**
 * @brief Signaling LED
 *
 * @note Typically on development kit is LED on GPIO2
 */
#define IO_LED (2)
// ===============================| Advanced |================================
/**
 * @brief I2C clock speed in bits per second
 *
 * Need to work at relatively high bitrate, because even at 1 KHz read rate,
 * it make quite difference. Note that bus rate is in bits and there is some
 * extra overhead due to extra communication (request to read from registers,
 * set address and then possible to read)
 */
#define I2C_SCL_SPEED (400000)

/**
 * @brief UART interface for sensors
 *
 * Note that one UART is used by ESP's log system, another is used by service
 * UI, so this is the last one, which print raw/debug data
 */
#define SENSORS_UART_INTERFACE (UART_NUM_2)

/**
 * @brief RX buffer size for sensor's UART
 *
 * Although RX way is not used, it is required to have at least some buffer,
 * so here we go.
 * For some reason value lower than 128 is not allowed.
 */
#define UI_SENSORS_RX_BUFFER_SIZE (130)

/**
 * @brief Bitrate for UART which reports raw data from sensors
 *
 * This have to be relative high speed in order to not block other processing
 */
#define UI_SENSORS_UART_BAUDRATE (460800)
/**
 * @brief Number of iterations when calibrating gyroscope
 *
 * It is pointless to collect tons of data, since at the end it will drift
 * away.
 * The 1000 is good for some serious work, but if you do not need it, reduce
 * it significantly in order to speed up initialization time
 */
#define GYRO_CALIBRATION_ITERATIONS (100)

/**
 * @brief TX buffer size for output UART for sensors
 *
 * In order to avoid blocking as much as possible, use buffered approach
 */
#define SENSORS_TX_BUFFER_SIZE (1024)

/**
 * @brief Service UI IO
 *
 * @{
 */
/**
 * @brief Service UI baudrate
 */
#define UI_SERVICE_UART_BAUDRATE (115200)

/**
 * @brief Service UI UART interface
 */
#define UI_SERVICE_UART_INTERFACE (UART_NUM_1)

/**
 * @brief Service UI TX pin
 */
#define UI_SERVICE_TXD_PIN (GPIO_NUM_17)

/**
 * @brief Service UI RX pin
 */
#define UI_SERVICE_RXD_PIN (GPIO_NUM_16)
/**
 * @}
 */

/**
 * @brief Interface buffer size in Bytes
 *
 * Usually commands should be really short, so something like 256 should be
 * more than enough.
 *
 * @note For some reason, size under 128 is not allowed. Just be warned
 */
#define UI_SERVICE_ITF_BUFFER_SIZE (512)
// ============================| Default values |=============================
/**
 * @brief Define function which returns ms (32 bit value) as time reference
 */
#define GET_CLK_MS_32BIT() esp_log_timestamp()

// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
#endif  // __CFG_H__
