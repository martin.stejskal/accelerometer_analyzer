/**
 * @file
 * @author Martin Stejskal
 * @brief Simple button handler
 */
// ===============================| Includes |================================
#include "button.h"

#include <driver/gpio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/queue.h>
#include <freertos/task.h>

// ================================| Defines |================================
// Keep Eclipse happy
#ifndef IRAM_ATTR
#define IRAM_ATTR
#endif  // IRAM_ATTR
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
typedef struct {
  // Event queue
  xQueueHandle gpio_evnt_queue;

  // Callback for event "button pressed"
  tf_button_pressed_cb pf_button_pressed_cb;
} ts_runtime;
// ===========================| Global variables |============================

/**
 * @brief Runtime variables under one variable
 */
static ts_runtime ms_btn_runtime;

///!@brief Tag for logging system
static char* mp_tag = "Button manager";
// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================
/**
 * @brief Setup button GPIO
 *
 * Also link button to interrupt routine.
 */
static void _gpio_init(void);

/**
 * @brief Interrupt routine
 *
 * Send information about pressed button to RTOS queue
 *
 * @param pv_arg Pressed GPIO number
 */
static void IRAM_ATTR _gpio_isr_handler(void* pv_arg);

/**
 * @brief Exit button RTOS task gracefully
 *
 * Clean up link to ISR, gracefully close RTOS task
 */
static void _exit_task(void);
// =========================| High level functions |==========================
void button_task_rtos(void* pv_args) {
  // If any argument is given, it should be callback for "button pressed"
  if (pv_args) {
    button_set_cb((tf_button_pressed_cb)pv_args);
  } else {
    // Clean up callback
    ms_btn_runtime.pf_button_pressed_cb = 0;
  }

  // Setup GPIO & interrupt mode
  _gpio_init();

  uint32_t u32_gpio_num;
  uint32_t u32_button_pressed_time_ms = 0, u32_current_time_ms;

  while (1) {
    if (xQueueReceive(ms_btn_runtime.gpio_evnt_queue, &u32_gpio_num,
                      portMAX_DELAY)) {
      u32_current_time_ms = GET_CLK_MS_32BIT();

      if ((u32_current_time_ms - u32_button_pressed_time_ms) >
          BUTTON_DEBOUNCE_TIME_MS) {
        // Valid button press
        ESP_LOGI(mp_tag, "Button press at GPIO %d", u32_gpio_num);

        // If callback is set, call it. Otherwise nothing to do
        if (ms_btn_runtime.pf_button_pressed_cb) {
          ms_btn_runtime.pf_button_pressed_cb();
        } else {
          ESP_LOGI(mp_tag, "Callback not set. Nothing to do");
        }

      } else {
        // De-bouncing -> do nothing
        ESP_LOGI(mp_tag, "debouncing...");
      }

      u32_button_pressed_time_ms = u32_current_time_ms;

    } else {
      // Timeout - wait again
    }
  }

  _exit_task();
}
// ========================| Middle level functions |=========================
void button_set_cb(tf_button_pressed_cb pf_button_pressed) {
  ms_btn_runtime.pf_button_pressed_cb = pf_button_pressed;
}
// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
static void _gpio_init(void) {
  // Input button connects to GND. Therefore need pull up and react to
  // falling edge

  gpio_config_t s_gpio_cfg = {
      .pin_bit_mask = (1ULL << IO_START_STOP_BTN),
      .mode = GPIO_MODE_INPUT,
      .pull_up_en = 1,
      .pull_down_en = 0,
      .intr_type = GPIO_INTR_NEGEDGE,

  };

  gpio_config(&s_gpio_cfg);

  // GPIO values are passed as 32 bit values for some reason
  ms_btn_runtime.gpio_evnt_queue =
      xQueueCreate(BUTTON_EVENT_QUEUE_SIZE_ITEMS, sizeof(uint32_t));

  // Use default flags
  gpio_install_isr_service(0);

  gpio_isr_handler_add(IO_START_STOP_BTN, _gpio_isr_handler,
                       (void*)IO_START_STOP_BTN);
}

static void IRAM_ATTR _gpio_isr_handler(void* pv_arg) {
  uint32_t u32_gpio_num = (uint32_t)pv_arg;
  xQueueSendFromISR(ms_btn_runtime.gpio_evnt_queue, &u32_gpio_num, NULL);
}

static void _exit_task(void) {
  gpio_isr_handler_remove(IO_START_STOP_BTN);

  vTaskDelete(NULL);
}
