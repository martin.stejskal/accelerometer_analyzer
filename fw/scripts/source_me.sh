#!/bin/bash
# Script which prepare environment, install dependencies under virtual
# environment and run application.
#
# Author:   Martin Stejskal

# ESP32 system variable
export IDF_PATH="/home/$USER/esp/esp-idf"

# Add ESP toolchain to the PATH
# Espressiv added some cool features, so souring export.sh do the job
# PATH="$PATH:/home/$USER/esp/xtensa-esp32-elf/bin:"$IDF_PATH"/tools"
source "$IDF_PATH"/export.sh

