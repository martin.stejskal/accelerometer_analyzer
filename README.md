# About

* Initial idea was to have device, which will capture raw data from 
  accelerometer and it still does.
* You can record some gestures and process data at PC for example and let
  nerual network to do it's job. Or analyze data more traditional way. Fully
  up to you.
* Sampling frequency from accelerometer is up to 1000 Hz and everything will be
  working smoothly.
* On top of that, project support tap detection. You're free to play with all
  parameters
* That is not everything! Added also orientation detection, where you define
  which position in 3D space (Theta and Phi angles) and API will tell you
  whether accelerometer is in that position or not. Of course, there are
  parameters, which define also some tolerances, since it would be impractical
  to strictly require 1 degree accuracy.
  
# HW

* Based on `ESP32` and `MPU6050`. However, in general, any supported 3 axis
  sensor can be used. Take a look into
  [list of supported drivers](https://gitlab.com/martin.stejskal_esp32_modules/sensors/-/tree/master/drivers)
* Required another UART interface connected to PC (`115200-8-N-1`)

# FW

* Refer to [fw/README.md](fw/README.md)
* There is description of I/O settings, which is basically only thing you need
  to know.

# SW

* None. Use any data processor, like
  [Libreoofice Calc](https://www.libreoffice.org/discover/calc/)
